package org.campus.game;

public class Player
{
	private int number;
	private String name;
	
	public Player(String name)
	{
		this.name=name;
	}
	public String getName()
	{
		return name;
	}
	
	public int guess()
	{
		number= (int)( Math.random() *9);
		System.out.printf(" %s has suggest %d",name,number);
		return number;
	}

}
